{config, lib, pkgs, resources, ...}:
with lib;

let
  cfg = config.modules.reverseProxy;

  empty = list: builtins.length list == 0;
  stringContains = input: check: !(stringLength input == (stringLength (replaceStrings [check] [""] input)));
  machineIsBackend = name: machine: machine.modules.reverseProxy.backend.enable && cfg.clusterName == machine.modules.reverseProxy.clusterName;
  machineIsFrontend = name: machine: machine.modules.reverseProxy.frontend.enable;
  backendMachines = filterAttrs machineIsBackend resources.machines;
  frontendMachines = filterAttrs machineIsFront resources.machines;
  serviceNames = unique (foldl' (l: sset: l ++ (attrNames sset)) [] (map (machine: attrByPath ["modules" "reverseProxy" "backend" "services"] {} machine) (attrValues backendMachines)));

  machineDoesNotProvideService = service: name: !(hasAttr service resources.machines.${name}.modules.reverseProxy.backend.services);
  getMachinesProvidingService = service: removeAttrs backendMachines (filter (machineDoesNotProvideService service) (attrNames backendMachines));
  getInstancesOfService = service: filter (o: o.enable) (map (machine: attrByPath ["modules" "reverseProxy" "backend" "services" service] {} machine) (attrValues (getMachinesProvidingService service)));
  getUpstreams = service: map (options: "${options.host}:${toString options.port}") (getInstancesOfService service);
  getServiceOptions = service: removeAttrs (foldl' (r: s: r // s) {} (getInstancesOfService service)) ["host" "port" "enable"];
  getSshService = services: head (attrValues (filterAttrs (n: v: v.ssh) services));

  proxyServices = listToAttrs (map (name: {name = name; value = getServiceOptions name;}) serviceNames);

  getSubdomainFromFQDN = fqdn: head (splitString "." fqdn);
  getDomainFromFQDN = fqdn: last (init(splitString "." fqdn));
  getTldFromFQDN = fqdn: last (splitString "." fqdn);

  fqdnAllowedBySubdomains = subdomains: fqdn: ((builtins.length subdomains) == 0) || (builtins.elem (getSubdomainFromFQDN fqdn) subdomains);
  fqdnAllowedByDomains = domains: fqdn: ((builtins.length domains) == 0) || (builtins.elem (getDomainFromFQDN fqdn) domains);
  fqdnAllowedByTlds = tlds: fqdn: ((builtins.length tlds) == 0) || (builtins.elem (getTldFromFQDN fqdn) tlds);
  fqdnAllowed = tlds: domains: subdomains: fqdn: (fqdnAllowedByTlds tlds fqdn) && (fqdnAllowedByDomains domains fqdn) && (fqdnAllowedBySubdomains subdomains fqdn);
  filterServices = tlds: domains: subdomains: services: filterAttrs (n: v: (cfg.frontend.enable && (!empty (getInstancesOfService n)) && (fqdnAllowed tlds domains subdomains n))) services;

  mkHiddenServiceForService = name: options: ''
    HiddenServiceDir /var/lib/tor/${name}
    ${concatMapStringsSep "\n" (upstream:
    "HiddenServicePort ${if options.upstreamSSL then "443" else "80"} ${upstream}") (getUpstreams name)}
    ${optionalString options.ssh "HiddenServicePort 22 ${options.sshHost}:${toString options.sshPort}"}
  '';

  mkVhostFromService = name: options: let
    acme = !options.upstreamSSL && options.ssl && options.acme;
  in {
    enableSSL = !options.upstreamSSL && options.ssl;
    port = if options.ssl || options.upstreamSSL then 443 else 80;
    enableACME = acme;
    extraConfig = ''
    ${if options.external then "allow all;" else ''
        allow ${cfg.frontend.adminIP};
        ${optionalString (!options.admin) ''
          allow ${cfg.frontend.lanCIDR};
        ''}
        allow 127.0.0.1;
        deny all;
      ''}
      error_log syslog:server=unix:/dev/log;
      access_log syslog:server=unix:/dev/log;
      ${optionalString options.upstreamSSL "proxy_ssl_verify off;"}
    '';
    locations = {
      "/" = {
        proxyPass = "http${optionalString options.upstreamSSL ''s''}://${replaceStrings ["." "*"] ["-" "star"] name}-upstream/";
      };
    };
    sslCertificate = optionalString (options.ssl && !options.upstreamSSL && !acme) cfg.frontend.sslCert;
    sslCertificateKey = optionalString (options.ssl && !options.upstreamSSL && !acme) cfg.frontend.sslKey;
  };
in {
  options = {
    modules.reverseProxy = {
      clusterName = mkOption {
        default = "rproxy";
        type = types.str;
        description = "Name of the cluster to which this member should contribute.";
      };
      backend = let
        serviceOptions = { name, ... }: {
          options = {
            enable = mkOption {
              type = types.bool;
              default = true;
              description = "Enable this service backend.";
            };
            port = mkOption {
              type = types.int;
              default = 80;
              description = "Port of the backend web service to provide a frontend for.";
            };
            host = mkOption {
              type = types.str;
              default = config.networking.publicIPv4;
              description = "IP/hostname of the backend web service to provide a frontend for.";
            };
            external = mkOption {
              type = types.bool;
              default = false;
              description = "Make this site externally available with a .com tld.";
            };
            ssl = mkOption {
              type = types.bool;
              default = false;
              description = "Enable https on the frontend.";
            };
            upstreamSSL = mkOption {
              type = types.bool;
              default = false;
              description = "Passthrough ssl from the upstream hosts.";
            };
            acme = mkOption {
              type = types.bool;
              default = false;
              description = "Enable acme https certificate generation on the frontend.";
            };
            admin = mkOption {
              type = types.bool;
              default = false;
              description = "Only accessable via the admin interface/proxy.";
            };
            clearnet = mkOption {
              type = types.bool;
              default = true;
              description = "If this service can be reached over clear net.";
            };
            hideable = mkOption {
              type = types.bool;
              default = false;
              description = "If this service can have a hidden service frontend.";
            };
            ssh = mkOption {
              type = types.bool;
              default = false;
              description = "If this service has an ssh component to provide.";
            };
            sshHost = mkOption {
              type = types.str;
              default = config.networking.publicIPv4;
              description = "Host providing the ssh service.";
            };
            sshPort = mkOption {
              type = types.int;
              default = 22;
              description = "Port providing the SSH service.";
            };
          };
        };
      in {
        enable = mkOption {
          type = types.bool;
          default = false;
          description = "Enable the reverse proxy backend.";
        };
        services = mkOption {
          default = {};
          type = types.attrsOf (types.submodule serviceOptions);
          description = "Attribute set of services this host provides backends for with fqdn's as names.";
        };
      };
      frontend = {
        enable = mkOption {
          type = types.bool;
          default = false;
          description = "Enable the reverse proxy frontend.";
        };
        web = mkOption {
          type = types.bool;
          default = true;
          description = "Make this frontend available via a http reverse proxy.";
        };
        tor = mkOption {
          type = types.bool;
          default = false;
          description = "Make this frontend available via tor through hidden services.";
        };
        status = mkEnableOption "Status page on localhost";
        defaultPath = mkOption {
          type = types.string;
          default = "/srv/www/default";
          description = "Webroot path for the default/fallback site.";
        };
        defaultIndex = mkOption {
          type = types.string;
          default = "index.html";
          description = "Index files for the default/fallback site.";
        };
        adminIP = mkOption {
          type = types.string;
          default = "192.168.122.1";
          description = "IP address or CIDR allowed to access admin sites.";
        };
        externalIP = mkOption {
          type = types.string;
          default = config.networking.publicIPv4;
          description = "IP for the reverse proxy to listen on.";
        };
        lanCIDR = mkOption {
          type = types.string;
          default = "192.168.1.0/24";
          description = "IP range allowed access to .local tld hosts.";
        };
        sslCert = mkOption {
          type = types.string;
          default = "/etc/ssl/cert.pem";
          description = "SSL certificate.";
        };
        sslDhparam= mkOption {
          type = types.nullOr types.path;
          default = null;
          description = "Dhparam file to use for ssl.";
        };
        sslKey = mkOption {
          type = types.string;
          default = "/etc/ssl/key.pem";
          description = "SSL certificate key.";
        };
        subdomainWhitelist = mkOption {
          type = types.listOf types.str;
          default = [];
          description = "Whitelist of subdomains this frontend provides. When empty all are provided.";
        };
        domainWhitelist = mkOption {
          type = types.listOf types.str;
          default = [];
          description = "Whitelist of domains this frontend provides. When empty all are provided.";
        };
        tldWhitelist = mkOption {
          type = types.listOf types.str;
          default = [];
          description = "Whitelist of top level domains this frontend provides. When empty all are provided.";
        };
      };
    };
  };

  config = let
    filteredServices = filterServices cfg.frontend.tldWhitelist cfg.frontend.domainWhitelist cfg.frontend.subdomainWhitelist proxyServices;
    webServices = filterAttrs (n: v: v.clearnet) filteredServices;
    web = cfg.frontend.enable && cfg.frontend.web;
    tor = cfg.frontend.enable && cfg.frontend.tor;
    ssh = web && (builtins.any (v: v.ssh) (attrValues filteredServices));
    sshService = getSshService webServices;
    backend = cfg.backend.enable;
    status = cfg.frontend.status && !tor;
  in mkIf (cfg.frontend.enable || cfg.backend.enable) {
    networking.firewall.allowedTCPPorts = flatten [
      (optionals web [ 80 443 ])
      (optional ssh 2222)
      (optional (backend && !web) (map (backend: backend.port) (attrValues cfg.backend.services)))
    ];
    boot.kernel.sysctl = mkIf web {
      "net.ipv4.ip_forward" = true;
    };
    services.nginx = mkIf web {
      enable = true;
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      sslDhparam = cfg.frontend.sslDhparam;
      appendHttpConfig = let
        makeUpstream = service: ''
          upstream ${replaceStrings ["." "*"] ["-" "star"] service}-upstream {
          ${concatMapStringsSep "\n" (upstream:
          "  server ${upstream};") (getUpstreams service)}
          }
        '';
        forceSslHosts = attrNames (filterAttrs (n: v: v.ssl) webServices);
      in ''
        server_names_hash_bucket_size 64;
        server {
          listen 80 default_server;
          server_name _;
          error_log syslog:server=unix:/dev/log;
          access_log syslog:server=unix:/dev/log;
          allow all;
          index ${cfg.frontend.defaultIndex};
          root ${cfg.frontend.defaultPath};
          ${optionalString status ''
          location /nginx_status {
            stub_status on;
            access_log off;
            allow 127.0.0.1;
            ${optionalString config.networking.enableIPv6 "allow ::1;"}
            deny all;
          }''}
        }
        server {
          listen 443 ssl http2 default_server;
          server_name _;
          error_log syslog:server=unix:/dev/log;
          access_log syslog:server=unix:/dev/log;
          allow all;
          ssl_certificate ${cfg.frontend.sslCert};
          ssl_certificate_key ${cfg.frontend.sslKey};
          index ${cfg.frontend.defaultIndex};
          root ${cfg.frontend.defaultPath};
        }
        ${concatMapStringsSep "\n" makeUpstream (attrNames webServices)}
        ${optionalString (!empty forceSslHosts) "
        server {
          listen 80;
          listen [::]:80;
          server_name ${concatStringsSep " " forceSslHosts};
          location / {
            return 301 https://$host$request_uri;
          }
        }
        "}
      '';
      virtualHosts = mapAttrs mkVhostFromService webServices;
    };
    services.tor = mkIf tor {
      enable = true;
      extraConfig = concatStringsSep "\n" (attrValues (mapAttrs mkHiddenServiceForService (filterAttrs (n: v: v.hideable) filteredServices)));
    };
    systemd.services = mkIf ssh {
      ssh-redir = {
        enable = true;
        description = "Redirect port 2222 to the backend ssh service";
        wantedBy = [ "multi-user.target" ];
        serviceConfig.ExecStart = "${pkgs.redir}/bin/redir -n --ident sshforward ${cfg.frontend.externalIP}:2222 ${sshService.sshHost}:${toString sshService.sshPort}";
      };
    };
    networking.extraHosts = let
      fqdnServices = filterAttrs (n: m: !(stringContains n "*")) filteredServices;
    in optionalString web ''
      ${concatMapStringsSep "\n" (n: "127.0.0.1 ${n}") (attrNames fqdnServices)}
    '';
  };
}
